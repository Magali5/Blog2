<?php

namespace App\Entity;


class Category
{
  public $id;
  public $title;
  public $article;
  public $publish;

  public function fromSQL(array $sql)
  {
    $this->id = $sql["id"];
    $this->title = $sql["title"];
    $this->category = $sql["article"];
    $this->publish = $sql["publish"];
  }

}