<?php

namespace App\Entity;


class User
{
  public $id;
  public $name;
  public $surname;
  public $mail;
  public $password;

  public function fromSQL(array $sql)
  {
    $this->id = $sql["id"];
    $this->name = $sql["name"];
    $this->surname = $sql["surname"];
    $this->mail = $sql["mail"];
    $this->password = $sql["password"];
  }
}