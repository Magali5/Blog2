<?php

namespace App\Entity;


class Article
{
  public $id;
  public $title;
  public $category;
  public $date;
  public $content;
  public $draft;
  public $publish;

  public function fromSQL(array $sql)
  {
    $this->id = $sql["id"];
    $this->title = $sql["title"];
    $this->category = $sql["category"];
    $this->date = new \DateTime($sql["date"]);
    $this->content = $sql["content"];
    $this->draft = $sql["draft"];
    $this->publish = $sql["publish"];
  }

}