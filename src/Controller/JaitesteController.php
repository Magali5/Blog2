<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class JaitesteController extends AbstractController
{

  /**
   * @Route("/Jaiteste", name="jaiteste")
   */
  public function templateCategorie(ArticleRepository $repo)
  {
    $variable = $repo->getAll(); // va récupérer les données rentrées dans le  formulaire 

    return $this->render('jaiteste.html.twig', [
        'variable' => $variable,
    ]);
  }
}