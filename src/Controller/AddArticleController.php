<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Form\ArticleForm;




class AddArticleController extends Controller
{

  /**
   * @Route("/AddArticle", name="addArticle")
   */
  public function index(ArticleRepository $repo, Request $request)
  {
    $article = new Article;

    $form = $this->createForm(ArticleForm::class);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      
    // $article = $form->getData();
    // $repo->add($article);//on ajoute les infos dans le repo que l'on vient de faire 
      $repo->add($form->getData());

      return $this->redirectToRoute("jaiteste");
    }

    return $this->render('addArticle.html.twig', [
      'article' => $article,
      'form' => $form->createView()//permet d'afficher ce que l'on a rentré dans le formulaire
    ]);
  }
}