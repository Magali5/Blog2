<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class ArticleController extends AbstractController{

/**
   * @Route("/Article/{id}", name="article")
   */
  public function form( int $id, ArticleRepository $repo)
  {
    $variable = $repo->get($id); // va récupérer les données rentrées dans le formulaire
    return $this->render("_templateArticle.html.twig", ['variable' => $variable]);
  
  } 
}