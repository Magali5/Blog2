<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class ContactController extends AbstractController
{

  /**
   * @Route("/Contact", name="contact")
   */
  public function form(ArticleRepository $repo)
  {
    return $this->render("contact.html.twig", []);
  }
}