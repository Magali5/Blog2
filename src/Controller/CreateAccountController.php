<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CreateAccountController extends AbstractController
{

  /**
   * @Route("/CreateAccount", name="createAccount")
   */
  public function form()
  {
    return $this->render("createAccount.html.twig", []);
  }
}