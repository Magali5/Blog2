<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ConnexionController extends AbstractController
{

  /**
   * @Route("/Connexion", name="connexion")
   */
  public function form()
  {
    return $this->render("connexion.html.twig", []);
  }
}