<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class AsideController extends AbstractController{

  public function form(ArticleRepository $repo)
  {
    $variable = $repo->aside(); // va récupérer les données rentrées dans le formulaire
    return $this->render("_aside.html.twig", ['variable' => $variable]);
  } 
}