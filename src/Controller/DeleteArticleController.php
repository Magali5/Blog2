<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;

class DeleteArticleController extends AbstractController
{

  /**
   * @Route("/DeleteArticle/{id}", name="delete")
   */
  public function form(int $id, ArticleRepository $repo)
  {
    $variable = new Article;
    if ($variable !== 0) {
      
        $repo->delete($id);
  
        return $this->redirectToRoute("jaiteste");
      }
    return $this->render("_templateCategorie.html.twig", ['variable' => $variable]);
  }


}