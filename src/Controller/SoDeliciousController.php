<?php namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class SoDeliciousController extends AbstractController{
/**
 * @Route ("/SoDelicious", name="sodelicious")
 */
public function delicious(ArticleRepository $repo){
  $variable = $repo->getAll(); // va récupérer les données rentrées dans le  formulaire 

    return $this->render('soDelicious.html.twig', [
        'variable' => $variable,
        ]);
      }
}
