<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class SaltController extends AbstractController
{

  /**
   * @Route("/Salt", name="salt")
   */
  public function templateCategorie(ArticleRepository $repo)
  {
    $variable = $repo->cat("salé"); // va récupérer les données rentrées dans le  formulaire 

    return $this->render('salt.html.twig', [
        'variable' => $variable,
    ]);
  }
}

