<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Form\ArticleForm;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class UpdateArticleController extends AbstractController
{
  /**
   * @Route("/update/{id}", name="update")
   */

  public function update(int $id, ArticleRepository $repo, Request $request)
  {
    $article = $repo->get($id);

    $form = $this->createForm(ArticleForm::class, $article);// la on rentre le nom du formulaire que l'on a fait dans le dossier form

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

      $article = $form->getData();
      $repo->update($article);
      return $this->redirectToRoute("jaiteste");
    }

    return $this->render('updateArticle.html.twig', [
      'article' => $article,
      'form' => $form->createView()
    ]);
  }
}

