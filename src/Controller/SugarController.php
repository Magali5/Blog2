<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class SugarController extends AbstractController
{

  /**
   * @Route("/Sugar", name="sugar")
   */
  public function templateCategorie(ArticleRepository $repo)
  {
    $variable = $repo->cat("sucré"); // va récupérer les données rentrées dans le  formulaire 

    return $this->render('sugar.html.twig', [
        'variable' => $variable,
    ]);
  }
}

