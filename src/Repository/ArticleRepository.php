<?php

namespace App\Repository;

use App\Entity\Article;
use App\Utils\ConnectUtil;


//but : prendre les données brutes de la bases de données et les modifier en instance d'article
class ArticleRepository
{
  public function getAll() : array
  {// on convertit le sql en instance d'article
    $articles = [];
    try { //déclenche une erreur si il y en a une 
      $cnx = ConnectUtil::getConnection();

      $query = $cnx->prepare("SELECT * FROM article"); //nom de la base

      $query->execute(); //lance la méthode execute qui déclenche la requète

      foreach ($query->fetchAll() as $row) { // transforme le tableau de résultat et renvoie tous les résultats sous la forme d'un tableau associatif
        $article = new Article();
        $article->fromSQL($row);
        $articles[] = $article;
      }

     // dump($query->fetchAll());
    } catch (\PDOExeption $e) {
      dump($e);
    }
    return $articles;
  }

  public function cat(string $category) : array
  {// on convertit le sql en instance d'article
    $articles = [];
    try { //déclenche une erreur si il y en a une 
      $cnx = ConnectUtil::getConnection();

      $query = $cnx->prepare("SELECT * FROM article WHERE category = :category"); //nom de la base

      $query->bindValue(":category", $category);
      $query->execute(); //lance la méthode execute qui déclenche la requète

      foreach ($query->fetchAll() as $row) { // transforme le tableau de résultat et renvoie tous les résultats sous la forme d'un tableau associatif
        $article = new Article();
        $article->fromSQL($row);
        $articles[] = $article;
      }

     // dump($query->fetchAll());
    } catch (\PDOExeption $e) {
      dump($e);
    }
    return $articles;
  }

  public function get($id) : Article
  {// on convertit le sql en instance d'article
    $articles = [];
    try { //déclenche une erreur si il y en a une 
      $cnx = ConnectUtil::getConnection();

      $query = $cnx->prepare("SELECT * FROM article WHERE id = :id"); //nom de la base

      $query->bindValue(":id", $id);
      $query->execute(); //lance la méthode execute qui déclenche la requète

      foreach ($query->fetchAll() as $row) { // transforme le tableau de résultat et renvoie tous les résultats sous la forme d'un tableau associatif
        $article = new Article();
        $article->fromSQL($row);
        $articles[] = $article;
      }

    } catch (\PDOExeption $e) {
      dump($e);
    }
    return $articles[0];
  }

  public function add(Article $article)
  {
    // on convertit une instance de article en sql
    try {
      $cnx = ConnectUtil::getConnection();
      $query = $cnx->prepare("INSERT INTO article(title, category, date, content, draft, publish)VALUES(:title, :category, :date, :content, :draft, :publish)");


      $query->bindValue(":title", $article->title);
      $query->bindValue(":category", $article->category);
      $query->bindValue(":date", $article->date->format("Y-m-d"));
      $query->bindValue(":content", $article->content);
      $query->bindValue(":draft", $article->draft);
      $query->bindValue(":publish", $article->publish);
      $query->execute();

      $article->id = intval($cnx->lastInsertId());//lastInsertId est une méthode de PDO on le fait uniquement dans le cas de l'insert into
    } catch (\PDOException $e) {
      dump($e);
    }
  }


  public function delete($id)
  {
    try { //déclenche une erreur si il y en a une 
      $cnx = ConnectUtil::getConnection();

      $query = $cnx->prepare("DELETE FROM article WHERE id = :id");
      $query->bindValue(":id", $id);
      $query->execute();

    } catch (\PDOExeption $e) {
      dump($e);
    }

  }
  public function update(Article $article)
  {
    try {
      $cnx = ConnectUtil::getConnection();
      $query = $cnx->prepare("UPDATE article SET title=:title, category=:category, date=:date, content=:content, draft=:draft, publish=:publish WHERE id=:id");
      $query->bindValue(":id", $article->id);
      $query->bindValue(":title", $article->title); //là on rentre des instances de classe car c'est ce qui est attendu dans les paramètres de la fonction 
      $query->bindValue(":category", $article->category);
      $query->bindValue(":date", $article->date->format("Y-m-d"));
      $query->bindValue(":content", $article->content);//permet d'assigner des valeurs au "placeholder"
      $query->bindValue(":draft", $article->draft);
      $query->bindValue(":publish", $article->publish);
      $query->execute();

    } catch (\PDOException $e) {
      dump($e);
    }
  }

  public function aside() : array
  {// on convertit le sql en instance d'article
    $articles = [];
    try { //déclenche une erreur si il y en a une 
      $cnx = ConnectUtil::getConnection();

      $query = $cnx->prepare("SELECT * FROM article LIMIT 5"); //nom de la base

      $query->execute(); //lance la méthode execute qui déclenche la requète

      foreach ($query->fetchAll() as $row) { // transforme le tableau de résultat et renvoie tous les résultats sous la forme d'un tableau associatif
        $article = new Article();
        $article->fromSQL($row);
        $articles[] = $article;
      }

     // dump($query->fetchAll());
    } catch (\PDOExeption $e) {
      dump($e);
    }
    return $articles;
  }
}
 
